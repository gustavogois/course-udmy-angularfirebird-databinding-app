import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-event-binding',
  templateUrl: './event-binding.component.html',
  styleUrls: ['./event-binding.component.css']
})
export class EventBindingComponent implements OnInit {

  buttonName = 'My button';
  i = 0;
  spinnerMode = "determinate";
  btnEnabled = true;
  selectDisable = false;
  selectedOption = 1;
  inputName = "";

  constructor() { }

  ngOnInit(): void {
  }

  save() {
    console.log("Click!");
  }

  inc() {
    this.i++;
    this.buttonName = "Foi clicado " + this.i + " vezes";
  }

  disable() {
    this.btnEnabled = false;
    this.spinnerMode = "indeterminate";
    setTimeout( () => {
      this.btnEnabled = true;
      this.spinnerMode = "determinate";
    }, 3000);
  }
  cbChange(event) {
    console.log(event);
    this.selectDisable = event.checked;
  }

  selectionChange(event) {
    console.log(event);
    this.selectedOption = event.value;
  }

  /*inputEvent(event) {
    console.log(event);
  }*/
}
